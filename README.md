![Build Status](https://code.fbi.h-da.de/cspub/cma/-/pipelines)

---

[Hugo][] [Docsy][] website using GitLab Pages.

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

**Table of Contents**

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [Preview your site](#preview-your-site)


## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project (Make sure to also recursively clone the submodules)
2. [Install][] Hugo extended version
3. Preview your project on local host zsing the command `hugo server`
4. Add or edit content etc.
5. Generate the website locally (optional) with the command `hugo --minify --gc --destination public`

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313`.


[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[docsy]: https://github.com/google/docsy/
