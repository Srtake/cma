---
title: "Projects and Initiatives"
linktitle: "Projects and Initiatives"
date: 2021-05-06T00:12:55+02:00
draft: false
type: docs
weight: 6
menu:
  main:
    weight: 2
---
- [Open Quantum Safe](https://openquantumsafe.org/):
An open-source project that aims to support the development and prototyping of quantum-resistant cryptography.

- [Quantum RISC](https://www.quantumrisc.de/):
Next Generation Cryptography for Embedded Systems.

- [Eclipse CogniCrypt]( https://www.eclipse.org/cognicrypt/):
Secure Integration of Cryptographic Software.

- [BSI-Project: Secure Implementation of a Universal Crypto Library](https://www.bsi.bund.de/DE/Themen/Unternehmen-und-Organisationen/Informationen-und-Empfehlungen/Kryptografie/Kryptobibliothek-Botan/kryptobibliothek-botan_node.html) More information (in German language) can be found in the [project summary](https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Krypto/Projektzusammenfassung_Botan.pdf)
