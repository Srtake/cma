---
title: "About Cryptographic Migration & Agility"
linktitle: "About Cryptographic Migration & Agility"
date: 2021-05-05T22:17:54+02:00
draft: false
type: docs
weight: 1
menu:
  main:
    weight: 1
---
Post-quantum cryptographic schemes have been under development for several years. Very soon there will be standardized post-quantum algorithms replacing the previous standards, which will eventually become obsolete. In order for quantum-resistant cryptographic Measures to be utilized, one needs more than simply developing secure post-quantum algorithms. The migration towards PQC poses great challenges on different levels. Those are not only restricted to the integration into existing protocols, but also include performance issues such as hardware specifications and memory usage, and especially the uncertainty of long term security of the new algorithm families. Moreover, a major challenge lies within finding suitable means of communicating and negotiating new algorithms and protocol parameters between different IT-systems. This leads to the urgent need for establishing the concept of crypto-agility, so as to be prepared for the rapid changes of cryptography, and insure the compatibility in all possible scenarios and settings.
