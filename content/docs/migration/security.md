---
title: "Security Considerations"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 3
---
##### **Algorithm, Parameter Selection and Tradeoffs**
- Key/sig. size problematic for protocols:
  - [The Viability of Post-quantum X.509 Certificates](https://eprint.iacr.org/2018/063.pdf): Present suitable parameters for software signature use cases and good signature candidates for TLS 1.3 authentication. [[KPDG18]](../../refs#kpdg18)
  - [Towards post-quantum security for cyber-physical systems: Integrating PQC into industrial m2m communication](http://link.springer.com/10.1007/978-3-030-59013-0_15): Tradeoffs in security: big key/certificate sizes results in problems and difficulties for various protocols.[[PS20]](../../refs#ps20)

##### **Cryptanalysis**
- PQC schemes broken by cryptanalysis:
  - [Cryptanalysis of the Lifted Unbalanced Oil Vinegar Signature Scheme](https://eprint.iacr.org/2019/1490.pdf): A new type of attack called Subfield Differential Attack (SDA) on Lifted Unbalanced Oil and Vinegar (LUOV) [[DDS+20]](../../refs#dds20)
  - [Quantum cryptanalysis on some generalized Feistel schemes](https://eprint.iacr.org/2017/1249.pdf): Quantum distinguishers to introduce generic quantum key-recovery attacks [[DLW19]](../../refs#dlw19)
  - [A reaction attack against cryptosystems based on LRPC codes.](https://eprint.iacr.org/2019/845.pdf): Analyze cryptosystems based on Low-Rank Parity-Check (LRPC) codes. [[SSPB19]](../../refs#sspb19)
- New security assessment methods:
  - [Quantum Cryptanalysis in the RAM Model: Claw-Finding Attacks on SIKE.](https://eprint.iacr.org/2019/103.pdf): New models of computation which allow a direct comparison between classical and quantum algorithms [[JS19]](../../refs#js19)
  - [A classification of differential invariants for multivariate post-quantum cryptosystems](http://link.springer.com/10.1007/978-3-642-38616-9_11): Present an extension of a recent measure of security against a differential adversary. The technique assures security against any first-order differential invariant adversary. [[PST13]](../../refs#pst13)
- Code-based PQC algorithms for PRNG:
  - [Testing of Code-Based Pseudorandom Number Generators for Post-Quantum Application](https://www.researchgate.net/publication/342456148_Testing_of_Code-Based_Pseudorandom_Number_Generators_for_Post-Quantum_Application): Code-based pseudorandom generator, improvement of Fischer-Stern generator [[KKS+20]](../../refs#kks20)

##### **Side-Channel Attacks**
- Side-Channel Attacks:
  - [Physical security in the post-quantum era: A survey on side-channel analysis, random number generators, and physically unclonable functions](https://arxiv.org/abs/2005.04344): Overview of several PQC-related side-channel attacks[[CCA+21]](../../refs#cca21)
- Minimize attack vectors:
  - [Physical protection of lattice-based cryptography: Challenges and solutions](https://pure.qub.ac.uk/files/156772945/paper.pdf): Attack and countermeasure for gaussian sampler of lattice-based schemes. [[KOV+18]](../../refs#kov18)
  - [A side-channel resistant implementation of saber](https://eprint.iacr.org/2020/733.pdf): State of the art in terms of side channel attacks against lattice based cryptosystems and their respective countermeasures. [[VBDK+20]](../../refs#vbdk20)
  - [Side-Channel Analysis and Countermeasure Design on ARM-based Quantum-Resistant SIKE](https://ieeexplore.ieee.org/document/9181442): Side-Channel resistant implementation of saber, using masking as a countermeasure [[ZYD20]](../../refs#zyd20)
- Successfull attack on Himq-3:
  - [A complete cryptanalysis of the post-quantum multivariate signature scheme himq-3](https://link.springer.com/chapter/10.1007%2F978-3-030-61078-4_24): Singularity Attack: Successfully breaks signatures of the multivarite public key scheme Himq-3 [[DDW20]](../../refs#ddw20)
