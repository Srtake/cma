---
title: "PQC Algorithms"
date: 2021-05-05T22:41:49+02:00
draft: false
type: docs
weight: 1
---
The current state of PQC is represented by the ongoing [NIST PQC standardization process](https://www.nist.gov/pqcrypto)
- [Report on post-quantum cryptography](https://nvlpubs.nist.gov/nistpubs/ir/2016/nist.ir.8105.pdf) [[CJL+16]](../../refs#cjl16).
- [Status report on the first round](https://nvlpubs.nist.gov/nistpubs/ir/2019/NIST.IR.8240.pdf) [[AASA+19]](../../refs#aasa19).
- [ Status report on the second round](https://nvlpubs.nist.gov/nistpubs/ir/2020/NIST.IR.8309.pdf) [[MAA+20]](../../refs#maa20).

#### **NIST PQC candidate algorithms:**

| Algorithm                           | Description | Type | NIST Round |
|-------------------------------------|-------------|------|------------|
| [BIKE](https://bikesuite.org/) | Bit flipping key encapsulation based on QC-MDPC (Quasi-Cyclic Moderate Density Parity-Check) [[ABB+20]](../../refs#abb20) | Public-key Encryption and Key-establishment | Round Three Alternative |
| [CRYSTALS-Dilithium](https://pq-crystals.org/dilithium/) | Digital signature scheme based on the hardness of lattice problems over module lattices [[DKL+21]](../../refs#dkl21) | Digital Signature | Round 3 Finalist |
| [Falcon](https://falcon-sign.info/) | Lattice-based signature scheme based on the short integer solution problem (SIS) over NTRU lattices [[FHK+20]](../../refs#fhk+20) | Digital Signature | Round 3 Finalist |
| [FrodoKEM](https://frodokem.org/)| Key encapsulation from generic lattices [[NAB+20]](../../refs#nab+20) | Public-key Encryption and Key-establishment | Round Three Alternative |
| [GeMSS](https://www-polsys.lip6.fr/Links/NIST/GeMSS.html) | Multivariate signature scheme producing small signatures [[CFP+19]](../../refs#cfp19) | Digital Signature | Round Three Alternative |
| [HQC](http://pqc-hqc.org/) | Hamming quasi-cyclic code-based public key encryption scheme [[MAB+20]](../../refs#mab20) | Public-key Encryption and Key-establishment | Round Three Alternative |
| [KYBER](https://pq-crystals.org/kyber/) | IND-CCA2-secure key-encapsulation mechanism (KEM) based on hard problems over module lattices [[ABD+21]](../../refs#abd21)| Public-key Encryption and Key-establishment | Round 3 Finalist | 
| [Classic McEliece](https://classic.mceliece.org/) | Code-based public-key cryptosystem based on random binary Goppa codes [[CCU+20]](../../refs#ccu+20) | Public-key Encryption and Key-establishment | Round 3 Finalist | 
| [NTRU](https://ntru.org/) | Public-key cryptosystem based on lattice-based cryptography [[CDH+19]](../../refs#cdh19) | Public-key Encryption and Key-establishment | Round 3 Finalist | 
| [NTRU-Prime](https://ntruprime.cr.yp.to/) | Small lattice-based key-encapsulation mechanism (KEM) [[BBC+20]](../../refs#bbc20) | Public-key Encryption and Key-establishment | Round 3 Alternative | 
| [Picnic](https://microsoft.github.io/Picnic/) | Digital signature algorithems based on the zero-knowledge proof system and symmetric key primitives [[CDG+17]](../../refs#cdg17) | Digital Signature | Round 3 Alternative |
| [Rainbow](https://www.pqcrainbow.org/)| Public key cryptosystem based on the hardness of solving a set of random multivariate quadratic systems [[DS05]](../../refs#ds05) | Digital Signature | Round 3 Finalist |
| [SABER](https://www.esat.kuleuven.be/cosic/pqcrypto/saber/) | IND-CCA2-secure Key Encapsulation Mechanism (KEM) based on the hardness of the Module Learning With Rounding problem (MLWR) [[DKR+19]](../../refs#dkr+19) | Public-key Encryption and Key-establishment | Round 3 Finalist |
| [SIKE](https://sike.org/)| Isogeny-based key encapsulation suite based on pseudo-random walks in supersingular isogeny graphs [[CCH+20]](../../refs#cch20) | Public-key Encryption and Key-establishment | Round 3 Alternative |
| [SPHINCS+](https://sphincs.org/) | A stateless hash-based signature scheme [[BHK+19]](../../refs#bhk19) | Digital Signature | Round 3 Alternative |
||
| [NewHope](https://newhopecrypto.org/) | Key-exchange protocol based on the Ring-Learning-with-Errors (Ring-LWE) problem [[ADPS16]](../../refs#adps16) | Public-key Encryption and Key-establishment | Round Two |
| [qTESLA](https://qtesla.org/) | Signature schemes based on the hardness of the decisional Ring Learning With Errors (R-LWE) problem [[ABB+20]](../../refs#abb20) | Digital Signature | Round Two |
