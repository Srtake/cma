---
title: "How to Contribute"
linktitle: "How to Contribute"
date: 2021-05-05T22:18:28+02:00
draft: false
type: docs
weight: 9
---
Your contributions are always welcome!

To contribute to this site send a Pull Request to our repository on [Gitlab](https://code.fbi.h-da.de/cspub/cma)
#### Guidelines

* Use the development branch.
* Add one link per Pull Request.
    * Make sure the PR title is in the format of `Add project-name`.
    * Write down the reason why the contribution is suitable.
* Add the link: `* [project-name](http://example.com/) - A short description ends with a period.`
    * Keep descriptions concise and **short**.
* Add a section if needed.
    * Add the section description.
    * Add the section title to Table of Contents.
* Search previous Pull Requests or Issues before making a new one, as yours may be a duplicate.
* Check your spelling and grammar.
* Remove any trailing whitespace.
