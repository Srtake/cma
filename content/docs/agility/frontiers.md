---
title: "Frontiers of Cryptography"
linktitle: "Frontiers of Cryptography"
date: 2021-05-06T00:12:24+02:00
draft: false
type: docs
weight: 5
---
- Blockchains difficult
  - [Stateful Hash-based Digital Signature Schemes for Bitcoin Cryptocurrency](https://ieeexplore.ieee.org/document/9043192) This research work presents basic analysis and the background understanding of Stateful Hash-based Signature Schemes, particularly the Lamport One-Time Signature Scheme, Winternitz One-Time Signature Scheme, and the Merkle Signature Scheme. [[NWAO19]](../../refs#nwao19)
- Satellites difficult
  - [Quantum Resistant Authentication Algorithms for Satellite-Based Augmentation Systems](https://web.stanford.edu/group/scpnt/gpslab/pubs/papers/Neish_2018_IONITM_QuantumResistantAuthenticationUpdated.pdf) Introduces the cryptographic primitives necessary to understand the vulnerabilities in modern day cryptography due to quantum computing and investigates the use of TESLA and EC-Schnorr algorithms in broadcast systems. [[NWE19]](../../refs#nwe19)
- Cryptographic primitives handable
  - [Cryptographic Agility and its Relation to Circular Encryption](https://eprint.iacr.org/2010/117) Researches whether wPRFs (weak-PRFs) are agile and whether every secure (IND-R) encryption scheme is secure when encrypting cycles. [[ABBC10]](../../refs#abbc10)
