---
title: "Modalities"
linktitle: "Modalities"
date: 2021-05-06T00:11:45+02:00
draft: false
type: docs
weight: 1
---
- New agile protocols:
  - [Security Agility Solution Independent of the Underlaying Protocol Architecture](https://www.semanticscholar.org/paper/Security-Agility-Solution-Independent-of-the-Vasic-Mikuc/489054a1f28eb26b1baa1a9f0caff2306c821695) The agilecryptographic negotiation protocol (ACNP) proposed in this paper repre-sents a layer-agnostic, robust solution that can be deployed for providingcryptographic agility and greatly improve security. [[VM12]](../../refs#vm12)
  - [Stateful Hash-based Digital Signature Schemes for Bitcoin Cryptocurrency](https://ieeexplore.ieee.org/document/9043192) This research work presents basic analysis and the background understanding of Stateful Hash-based Signature Schemes, particularly the Lamport One-Time Signature Scheme, Winternitz One-Time Signature Scheme, and the Merkle Signature Scheme. [[NWAO19]](../../refs#nwao19)
- Enhance existing protocols for use with PQC
  - [Public Key Cryptography for Initial Authentication in Kerberos (PKINIT) Algorithm Agility](https://tools.ietf.org/html/rfc8636.html) This document updates the Public Key Cryptography for Initial Authentication in Kerberos (PKINIT) standard (RFC 4556) to remove protocol structures tied to specific cryptographic algorithms. [[AZCH19]](../../refs#azch19)
  - [The Secure Socket API: TLS as an Operating System Service](https://www.usenix.org/conference/usenixsecurity18/presentation/oneill) We explore the use of the standard POSIX socket API as a vehicle for a simplified TLS API, while also giving administrators the ability to control applications and tailor TLS configuration to their needs. [[OHW+18]](../../refs#ohw18)
- Enhance existing infrastructure for PQC
  - [Algorithm Agility – Discussion on TPM 2.0 ECC Functionalities](https://link.springer.com/chapter/10.1007%2F978-3-319-49100-4_6) In this paper, we review all the TPM 2.0 ECC functionalities, and discuss on whether the existing TPM commands can be used to implement new cryptographic algorithms which have not yet been addressed in the specification. [[CU16]](../../refs#cu16)
  - [Fail-Safe-Konzept für Public-Key-Infrastrukturen](https://tuprints.ulb.tu-darmstadt.de/246/) In dieser Dissertation wird ein Fail-Safe-Konzept für Public-Key-Infrastrukturen vorgestellt. [[Mas02]](../../refs#mas02)
  - [Public Key Infrastructure and Crypto Agility Concept for Intelligent Transportation Systems](http://www.thinkmind.org/index.php?view=article&articleid=vehicular_2015_1_30_30028) This paper proposes a multi-domain PKI architecture for intelligent transportation systems, which considers the necessities of road infrastructure authorities and vehicle manufacturers, today. [[UWK15]](../../refs#uwk15)
- Draft for composite keys and signatures
  - [Composite Keys and Signatures For Use In Internet PKI](https://tools.ietf.org/id/draft-ounsworth-pq-composite-sigs-01.html) This document defines the structures CompositePublicKey, CompositeSignatureValue, and CompositeParams, which are sequences of the respective structure for each component algorithm. [[OP20]](../../refs#op20)
