---
title: "Testing"
linktitle: "Testing"
date: 2021-05-06T00:12:09+02:00
draft: false
type: docs
weight: 3
---
- Algorithm relations for better test coverage
  - [Systematic Testing of Post-Quantum Cryptographic Implementations Using Metamorphic Testing](https://ieeexplore.ieee.org/document/8785645) Investigates the effectiveness of a systematic testing approach for discovering bugs in highly complex cryptographic algorithm implementations. [[PRKK19]](../../refs#prkk19)
